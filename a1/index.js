let num = 2;
const getCube = Math.pow( num, 3);
console.log(`The cube of ${num} is ${getCube}`);

let fullAddress = ["258 Washington Ave", "NW,", "California","90011"]

let [street, city, state, zipCode] = fullAddress;

console.log(`I live at ${street} ${city} ${state} ${zipCode}`);



/*. 7.Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.*/


let animal = {
	species: "Animal",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
};

let {species, weight, measurement} = animal;

console.log(`Lolong was a saltwater ${species}. He weighted at ${weight} with a measurement of ${measurement}`);


/*9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

let arrNums = [1, 2, 3, 4, 5];
arrNums.forEach((num) => {
	console.log(num);
});
console.log(arrNums.reduce((x,y)=>x+y));



/*12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.*/

class Dog{
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};


let myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = 'Miniature Dacshund'

console.log(myDog);





