
console.log("Happy 12-12");

//ES6
// ES - ECMAScript - European Computer Manufacturers Association Script

// [SECTION] Exponent Operator

//Using exponent operator
const firstNum = 8 ** 2;
console.log(firstNum);

//Using Math Object methods
const secondNum = Math.pow(8, 2);

//[ SECTION ] Template Literals

let name = "John";
let message = "Hello " + name + "! \n Welcome to programming!";
console.log("Message without template literals: " + message);

message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

//Multi-line using template literals
const anotherMessage = `${name} attended a math competition. He won it by solving the problem 8**2 with the solution of ${secondNum} `;
console.log(anotherMessage);

// [SECTION] Array Destructuring

const fullName = ['Juan', 'Dela', 'Cruz'];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log("Hello " + fullName[0] + fullName[1] + fullName[2] + " It's nice to meet you ");


console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} It's nice to meet you  `)

//Array Destructuring

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName} It's nice to meet you`);

function getFullName([firstName,middleName,lastName]) {
	console.log(`${firstName} ${middleName} ${lastName}`)
};

getFullName(fullName);





//Object Destructuring
const person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz",

};

//Pre-Object Destructuring
console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName} It's good to see you again!`)

// Object Destructuring
const {givenName, midName, familyName } = person;

console.log(givenName);
console.log(midName);
console.log(familyName);
console.log(`Hello ${givenName} ${midName} ${familyName} It's good to see you again!`);


//Object destructuign can also be done inside the parameter
function displayFullName({givenName, midName, familyName}) {
	console.log(`${givenName} ${midName} ${familyName}`);
};

displayFullName(person);

//[SECTION] Arrow functions

function greeting() {
	console.log("Hi, Batch 244!");
};
greeting();


// //Pre - Arrow function
// function printFullName(firstName,middleName,lastName) {
// 	console.log(firstName + " " + middleName + " " + lastName );



// }
// printFullName("John", "D", "Smitch");


//Arrow Function 
/*- Compact alternative syntax to traditional functions
	- This will only work with "function expression".
	Syntax:
			let/const variableName = () =>{
				//code block/statement
			}

			let/const variableName = (parameter) =>{
				//code block/statement with parameter
			}
*/


const greeting1 = () =>{
	console.log(`Hi again, Batch 244!`);
}
greeting1();



const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}` )
} ;
printFullName("John", "D", "Smitch");


//Pre- Arrow Function with loops
const students =["Arn", 'Jans', 'Jerome'];

students.forEach(function (student) {
	console.log(`${student} is a student`);
});

//Arrow Function with loops
students.forEach((student) =>{
	console.log(`${student} is a student.`)
});


//[SECTION] Implicit Return Statement


//Pre-arrow function with "return" keyword
// const add = (x,y) =>{
// 	return x + y;
// };

// let total = add(1,2);
// console.log(total);




// Arrow function without the return keyword;
const add = (x,y) => x + y;

let total = add(1,2);
console.log(total);

//[SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'Cedrick') =>{
	return `Good Morning, ${name}!`;
}
console.log(greet());
// console.log(greet("Cedrick"));

//[SECTION] Class-based Object Blueprints
// {} - Object literal
//Object constructor

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

let myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptop";
myCar.year = 2022;
console.log(myCar);

let myDreamCar = new Car();
myDreamCar.brand = "Toyota";
myDreamCar.name = "Hilux Conquest";
myDreamCar.year = 2023;
console.log(myDreamCar);
























